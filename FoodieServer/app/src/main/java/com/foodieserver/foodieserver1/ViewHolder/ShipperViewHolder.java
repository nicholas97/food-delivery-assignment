package com.foodieserver.foodieserver1.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.foodieserver.foodieserver1.Interface.ItemClickListener;
import com.foodieserver.foodieserver1.R;

public class ShipperViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView shipper_name,shipper_phone;
    public Button btn_edit,btn_remove;

    private ItemClickListener itemClickListener;

    public ShipperViewHolder(@NonNull View itemView) {
        super(itemView);
        shipper_name=(TextView)itemView.findViewById(R.id.shipper_name);
        shipper_phone=(TextView)itemView.findViewById(R.id.shipper_phone);
        btn_edit=(Button)itemView.findViewById(R.id.btn_edit);
        btn_remove=(Button)itemView.findViewById(R.id.btn_remove);
    }

    @Override
    public void onClick(View v) {
        this.itemClickListener.onClick(v,getAdapterPosition(),false);

    }
    public void setItemClickListener(ItemClickListener itemClickListener){
        this.itemClickListener=itemClickListener;


    }
}
