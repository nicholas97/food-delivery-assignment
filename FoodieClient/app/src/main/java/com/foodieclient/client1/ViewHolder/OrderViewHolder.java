package com.foodieclient.client1.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.foodieclient.client1.Interface.ItemClickListener;
import com.foodieclient.client1.R;

/**
 * Created by 123456 on 2017/11/20.
 */

public class OrderViewHolder extends RecyclerView.ViewHolder{

    public TextView txtOrderId, txtOrderStatus, txtOrderphone, txtOrderAddress,txtOrderDate;

    private ItemClickListener itemClickListener;
    public ImageView btn_delete,btn_detail;

    public OrderViewHolder(View itemView){
        super(itemView);
        Log.i("Order viewholder","Start");
        txtOrderAddress = itemView.findViewById(R.id.order_address);
        txtOrderId = itemView.findViewById(R.id.order_id);
        txtOrderStatus = itemView.findViewById(R.id.order_status);
        txtOrderphone = itemView.findViewById(R.id.order_phone);
        txtOrderDate = itemView.findViewById(R.id.order_date);
        btn_delete=(ImageView)itemView.findViewById(R.id.btn_delete);
        btn_detail=(ImageView)itemView.findViewById(R.id.btn_detail);


    }


}
