package com.foodieclient.client1.Common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;

import com.foodieclient.client1.Model.Request;
import com.foodieclient.client1.Model.User;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by 123456 on 2017/11/17.
 */

public class Common {
    public static User currentUser;
    public static Request currentRequest;
    public static final String INTENT_FOOD_ID="FoodId";

    public static String convertCodeToStatus(String code){
        if(code.equals("0")){
            return "Placed";
        }else if(code.equals("1")){
            return "On the way";
        }else if(code.equals("2")){
            return "Shipping";
        }else{
            return "Shipped";
        }


    }

    public static final String DELETE="Delete";

    public static boolean isConnectedToInternet(Context context){
        ConnectivityManager  connectivityManager=(ConnectivityManager)context.getSystemService(context.CONNECTIVITY_SERVICE);
        if(connectivityManager!=null){
            NetworkInfo[] info=connectivityManager.getAllNetworkInfo();
            if(info!=null){
                for(int i=0;i<info.length;i++){
                    if(info[i].getState()==NetworkInfo.State.CONNECTED){
                        return true;

                    }

                }

            }

        }return false;

    }
    public static String getDate(long time){
        Calendar calendar=Calendar.getInstance(Locale.ENGLISH);
        calendar.setTimeInMillis(time);
        StringBuilder date=new StringBuilder(
                android.text.format.DateFormat.format("dd-MM-yyyy HH:mm",calendar)
                        .toString());
        return date.toString();

    }
}
